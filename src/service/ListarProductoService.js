import MasterService from "./MasterService";
import { conf } from "../Config.js";
const PRODUCTO_API_URL = conf.url.API_URL + "product";
class ListarProductoService {
    listAllProduct() {
        var CURRENT_API_URL = `${PRODUCTO_API_URL}/listAllProduct`;
        console.log(MasterService.getDataService(CURRENT_API_URL));
        return MasterService.getDataService(CURRENT_API_URL);
    }
    saveProduct(payload) {
        var CURRENT_API_URL = `${PRODUCTO_API_URL}/saveProduct`;
        return MasterService.postDataServiceJSONHeader(CURRENT_API_URL, payload);
    }
    updateProduct(payload) {
        console.log(payload);
        var CURRENT_API_URL = `${PRODUCTO_API_URL}/updateProduct/` + payload.pdtId;
        console.log(CURRENT_API_URL);
        return MasterService.putDataServiceJSONHeader(CURRENT_API_URL, payload);
    }
    deleteById(payload) {
        console.log(payload);
        var CURRENT_API_URL = `${PRODUCTO_API_URL}/deleteById/` + payload.pdtId;
        console.log(CURRENT_API_URL);
        return MasterService.deleteEmptyDataServiceJSONHeader(CURRENT_API_URL);
    }
}
export default new ListarProductoService();
