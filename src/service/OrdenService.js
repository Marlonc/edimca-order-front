import MasterService from "./MasterService";
import { conf } from "../Config.js";
const ORDEN_API_URL = conf.url.API_URL + "ord";
class OrdenService {
    saveOrd(payload) {
        var CURRENT_API_URL = `${ORDEN_API_URL}/saveOrd`;
        return MasterService.postDataServiceJSONHeader(CURRENT_API_URL, payload);
    }
}
export default new OrdenService();
