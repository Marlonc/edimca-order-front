import React, { useState, useRef, useEffect } from "react";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { InputTextarea } from "primereact/inputtextarea";
import { Dropdown } from "primereact/dropdown";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { OverlayPanel } from "primereact/overlaypanel";
import { Toast } from "primereact/toast";
import ListarProductoService from "../service/ListarProductoService";
import OrdenService from "../service/OrdenService";
const Orden = () => {
    const [lisOrden, setLisOrden] = useState(null);
    const [valueCedulaCliente, setValueCedulaCliente] = useState("");
    const [valueNombreCliente, setValueNombreCliente] = useState("");
    const [valueFecha, setValueDecha] = useState("");
    const [valuePrice, setValuePrice] = useState("");
    const [valueQuatity, setValueQuatity] = useState("");
    const op = useRef(null);
    const [listProduct, setListProduct] = useState(null);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const isMounted = useRef(false);
    const toast = useRef();
    const [productosDetalle, setProductosDetalle] = useState(null);
    let [productosDetalleData, setProductosDetalleData] = useState([]);
    const [displayBasic, setDisplayBasic] = useState(false);
    const [position, setPosition] = useState("center");

    const [valueTotal, setTotal] = useState(0);
    useEffect(() => {
        setValueCedulaCliente("");
        setValueNombreCliente("");
        setValueDecha("");
        setValuePrice("");
        setProductosDetalle([]);
        setProductosDetalleData([]);
        ListarProductoService.listAllProduct().then((valid) => {
            setListProduct(valid.data);
        });
    }, []);

    const onProductSelect = (e) => {
        console.log("onrpro : ", e.value.pdtPrice);
        let _newprod = {
            pdtId: e.value.pdtId,
            pdtName: e.value.pdtName,
            unitCost: e.value.pdtPrice,
            quantity: 0,
        };
        let _products = [...productosDetalle];
        _products.push(_newprod);
        setProductosDetalle(_products);
        op.current.hide();
        toast.current.show({ severity: "info", summary: "Producto seleccionado", detail: e.value.pdtName, life: 3000 });
    };

    const inputTextEditor = (props, field) => {
        return <InputText type="text" value={props.rowData[field]} onChange={(e) => onEditorValueChange(props, e.target.value)} />;
    };
    const codeEditor = (props) => {
        return inputTextEditor(props, "code");
    };
    const onEditorValueChange = (props, value) => {
        console.log("props value: ", props.props.value);

        let updatedProducts = [...props.props.value];
        productosDetalleData = updatedProducts;
        //  let iva = value * 0.12;
        let unitCost = updatedProducts[props.rowIndex]["unitCost"];
        let valortotal = unitCost * value;
        updatedProducts[props.rowIndex][props.field] = value;
        updatedProducts[props.rowIndex]["price"] = valortotal;
        setProductosDetalleData(updatedProducts);
        // setIva(iva);
        setTotal(valortotal);
        let suma = 0;
        productosDetalleData.map((objX) => {
            setValuePrice((suma += objX.price));
            return suma;
        });
        console.log(productosDetalleData);
    };

    const savePosDataOrder = () => {
        let dtoOrderPost = {
            cedulaCli: valueCedulaCliente,
            orderCreate: new Date(),
            orderPriceTotal: valuePrice,
            nameCli: valueNombreCliente,
            ordDetailList: productosDetalle,
        };
        console.log("dtoOrderPost: ", dtoOrderPost);
        OrdenService.saveOrd(dtoOrderPost).then((valid) => {
            if (valid.status === 200 || valid.status === 201) {
                toast.current.show({ severity: "info", summary: "Orden creada", detail: valid.data.orderNo, life: 3000 });
            } else {
                toast.current.show({ severity: "error", summary: "Orden no creada", life: 3000 });
            }
        });
    };

    return (
        <div className="grid">
            <Toast ref={toast} />
            <div className="col-12">
                <div className="card">
                    <h5>Orden</h5>
                    <div className="p-fluid formgrid grid">
                        <div className="field col-12 md:col-6">
                            <label htmlFor="cedula_cli">Cédula</label>
                            <InputText id="cedula_cli" type="text" onInput={(e) => setValueCedulaCliente(e.target.value)} />
                        </div>
                        <div className="field col-12 md:col-6">
                            <label htmlFor="name_cli">Nombre</label>
                            <InputText id="name_cli" type="text" onInput={(e) => setValueNombreCliente(e.target.value)} />
                        </div>
                        <div className="field col-12 md:col-6">
                            <label htmlFor="order_create">Fecha</label>
                            <InputText id="order_create" type="date" onInput={(e) => setValueDecha(e.target.value)} />
                        </div>
                        <div className="field col-12 md:col-6">
                            <label htmlFor="order_price_total">Total</label>
                            <InputText id="order_price_total" type="text" value={valuePrice} onInput={(e) => setValuePrice(e.target.value)} />
                        </div>
                        <div className="field col-12 md:col-6">
                            <Button label="Agregar Producto" className="mr-2 mb-2" onClick={(e) => op.current.toggle(e)} />
                            <OverlayPanel ref={op} appendTo={document.body} showCloseIcon id="overlay_panel" style={{ width: "450px" }}>
                                <DataTable value={listProduct} selection={selectedProduct} selectionMode="single" dataKey="pdtId" responsiveLayout="scroll" paginator rows={5} onSelectionChange={onProductSelect}>
                                    <Column field="pdtId" header="Code Prod." sortable headerStyle={{ minWidth: "10rem" }} />
                                    <Column field="pdtName" header="Nombre" sortable headerStyle={{ minWidth: "10rem" }} />
                                    <Column field="pdtPrice" header="Precio" sortable headerStyle={{ minWidth: "8rem" }} />
                                </DataTable>
                            </OverlayPanel>
                        </div>
                        <div className="field col-12 md:col-6">
                            <Button label="Generar" className="mr-2 mb-2" onClick={() => savePosDataOrder()} />
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-12">
                <div className="card">
                    <h5>Detalle </h5>
                    <DataTable value={productosDetalle} editMode="cell" dataKey="idDetalle">
                        <Column field="pdtId" header="Cod Prod." sortable headerStyle={{ minWidth: "10rem" }} />
                        <Column field="pdtName" header="Nombre" sortable headerStyle={{ minWidth: "10rem" }} />
                        <Column field="unitCost" header="Precio Unt."></Column>
                        <Column field="quantity" header="Cantidad" editor={(props) => codeEditor(props)}></Column>
                        <Column field="price" header="Sub Total" onchange={(props) => codeEditor(props)} value={setTotal}></Column>
                    </DataTable>
                </div>
            </div>
        </div>
    );
};
const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};
export default React.memo(Orden, comparisonFn);
